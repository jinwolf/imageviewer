// 1. load image
// 2. scale image on canvas

var viewer = (function() {
  document.addEventListener('DOMContentLoaded', function() {

    var mouseDown = false;

    var zoomIn = document.getElementById("zoomIn");
    var zoomOut = document.getElementById("zoomOut");
    var rotateR = document.getElementById("rotateR");
    var rotateL = document.getElementById("rotateL");
    var coordinate = document.getElementById("coordinate");

    var viewer = document.getElementById("viewer");
    var context = viewer.getContext('2d');
    var currentDim = {
      width: viewer.width,
      height: viewer.height,
      pageX: 0,
      pageY: 0,
      offsetX: 0,
      offsetY:0,
      lastOffsetX:0,
      lastOffsetY:0
    };

    var imageObj = new Image();
    imageObj.onload = function() {
      context.drawImage(this, 0, 0, currentDim.width, currentDim.height);
    };

    imageObj.src = 'image/main.jpg';

    viewer.addEventListener('click', function(e) {
      coordinate.innerHTML = 'Current position x:' + e.pageX + ' y:' + e.pageY;
      currentDim.pageX = e.pageX;
      currentDim.pageY = e.pageY;
    });

    viewer.addEventListener('mouseup', function(e) {
      mouseDown = false;
      currentDim.lastOffsetX = currentDim.offsetX;
      currentDim.lastOffsetY = currentDim.offsetY;
    });

    viewer.addEventListener('mousedown', function(e) {
      mouseDown = true;
      currentDim.pageX = e.pageX;
      currentDim.pageY = e.pageY;
    });

    viewer.addEventListener('mousemove', function(e) {

      if (mouseDown === true) {
        var offsetX = currentDim.lastOffsetX + e.pageX - currentDim.pageX;
        var offsetY = currentDim.lastOffsetY + e.pageY - currentDim.pageY;
        context.save();
        // context.clearRect(0, 0, viewer.width, viewer.height);
        context.drawImage(imageObj, offsetX, offsetY, currentDim.width, currentDim.height);
        context.restore();

        currentDim.offsetX = offsetX;
        currentDim.offsetY = offsetY;
      }
    });

    viewer.addEventListener('dblclick', function(e) {
      var count = 1;
      updateZoom(count, 1.01, e.pageX, e.pageY);
    });

    zoomIn.onclick = function() {

      var count = 1;
      updateZoom(count, 1.01);
    }

    zoomOut.onclick = function() {
      var count = 1;
      updateZoom(count, 0.99);
    }

    var updateZoom = function(count, rate, x, y) {

      if (count > 20) return;

      var offsetXRate = x / viewer.width;
      var offsetYRate = y / viewer.height;

      // update the currentDim
      currentDim.width *= rate;
      currentDim.height *= rate;

      var offsetX = x - currentDim.width * offsetXRate;
      var offsetY = y - currentDim.height * offsetYRate;

      context.drawImage(imageObj, offsetX,offsetY, currentDim.width, currentDim.height );

      count++;

      setTimeout(function() {
        updateZoom(count, rate, x, y);
      }, 20);
    };
  });
})();
